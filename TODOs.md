# SimpleScheduler TODOs

## features

parsing / Control Flow Generator

MetaData

container sets
device groups

extensible Modules
- liquid handling
- cultivation
- assays
- molecular biology
- chemical synthesis

- subroutines

language:
- simple to learn syntax (close to english)
- readable
- variables 
- conditions
- loops
- modules
- concurrence
- high-level abstraction to low level step-by-step modelling

- specify optimisation critrerium
- ptimization criteria are calcu-
lated by the scheduling algorithm. Examples include
maximum sample throughput, minimum sample processing
duration, and minimum reagent consumption.

- recording each decision / step for later recovery of crash / power outage / ...
- long time processes (days/weeks/months)
- time constraints
- priorities
- extensive error handling / device errors / alternative routes
- - message system / notes / e-mails / logging
- converter from other languages



## implementation

 * comments
 * variables
 * functions / submodules
 * conditions
 * loops
 * 