# pythonLabScheduler

This is a process scheduling framework for the pythonLab process description language.

The framework is designed to be very generic:
The core scheduling algorithm can be exchanged by any scheduling concept that shall be developed, tested or used in a real world application.

## scheduling framework overview

![pythonLabScheduler](docs/images/pythonlabscheduler/pythonLabScheduler.svg)



## Core Features

 * macroscopic (days, calendar), mesoscopic (hours-days) and microscopic processes (e.g. process on a   measurement device) should be possible to schedule (with different demands on time-accuracy)
 * abstraction of resources
 * mapping of abstract resources to real-world object at runtime
 * handling of container sets and device groups
 * high-level abstraction to low level step-by-step modelling
 * concurrence / sequential execution
 * optimisation criteria specifyable, e.g. maximum sample throughput, minimum sample processing
   duration, and minimum reagent consumption
 * time constraints
 * priorities concept for parallel executing processes 
 * recording each decision / step for later recovery of crash / power outage / ...
 * extensive error handling / device errors / alternative routes
 * suitable also for long time processes (days/weeks/months)
 * process metadata throuhg lab description language (pythonLab)
 * tight coupling to [pythonLab](https://gitlab.com/opensourcelab/pythonLab)
 * parsing / Control Flow Generator
 * message system / notes / e-mails / logging
