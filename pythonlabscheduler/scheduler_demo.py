"""
________________________________________________________________________

:PROJECT: _

*brief summary*

:details: :
		  - bullet points

:author:  mark doerr <mark.doerr@uni-greifswald.de> : contrib.

:date: (creation)          20210306
.. note:: some remarks
.. todo:: -

________________________________________________________________________

**Copyright**:
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
  For further Information see COPYING file that comes with this distribution.
________________________________________________________________________

"""

__version__ = "0.0.1"

import logging

from simple_scheduler import SimpleScheduler
from resource_mapper import ResourceMapper
from round_robin_scheduler import RoundRobinScheduler

import demo_process

res_map = ResourceMapper()
res_map.resource_map = { "incubator1": cyt2_sila_server}

simple_sched = SimpleScheduler(res_map)

simple_sched.set_scheduler_implementation(RoundRobinScheduler())

simple_sched.add_process(filename="demo_incubation_process")
simple_sched.init()
simple_sched.run()

simple_sched.shutdown()
