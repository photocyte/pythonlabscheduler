#!/usr/bin/env python3
# vim:fileencoding=utf-8
"""
________________________________________________________________________

:PROJECT: _

*brief summary*

:details: :
		  - bullet points

:author:  mark doerr <mark.doerr@uni-greifswald.de> : contrib.

:date: (creation)          20210306
.. note:: some remarks
.. todo:: -

________________________________________________________________________

**Copyright**:
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
  For further Information see COPYING file that comes with this distribution.
________________________________________________________________________

"""

__version__ = "0.0.1"

import logging
import time

from round_robin_scheduler import RoundRobinScheduler

class SimpleScheduler:

    _resource_mapper = None

    default_step_delay = 1  # in seconds

    def __init__(self):
        self.process_steps = []  # simple list, later graph

    def init(self,
             scheduler_implementation,
             resource_mapper):
        self.procedures = []

        self.scheduler_implementation = RoundRobinScheduler()

    def _inject_scheduler_implementation(self,
                               scheduler_implementation
                               ) -> None:
        """
        Dependency injection of the implementation used.
            Allows to set the class used for simulation/real mode.

        :param implementation: A valid implementation of the HelloSiLA2_full_v3_3Servicer.
        """
        self.scheduler_implementation = scheduler_implementation

    def add_procedurce(self, process: str):
        procedure = self.parse2CFG()
        self.procedures.append(procedure)

    def add_step(self, step, events):
        self.process_steps.append((step, events))

    @resource_mapper.setter
    def resource_mapper(self, resource_mapper):
        self._resource_mapper = resource_mapper

    @property
    def resource_mapper(self):

        return self._resource_mapper

    def parse2CFG(self):
        pass

    def run(self):  # use thread and signal to
        for step, events in self.process_steps:

            # execute step
            try:
                time.sleep(self.default_step_delay)
            except Exception as err:
                logging.error(f"ERROR: {err}")

            # if signal pause -> pause run
            # if signal stop -> end process

    def pause(self):
        pass
        # send pause signal

    def resume(self):
        pass
        # send pause signal

    def stop(self):
        pass
        # send stop signal
