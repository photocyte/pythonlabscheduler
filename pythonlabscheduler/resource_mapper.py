"""
________________________________________________________________________

:PROJECT: _

*brief summary*

:details: :
		  - bullet points

:author:  mark doerr <mark.doerr@uni-greifswald.de> : contrib.

:date: (creation)          20210306
.. note:: some remarks
.. todo:: -

________________________________________________________________________

**Copyright**:
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
  For further Information see COPYING file that comes with this distribution.
________________________________________________________________________

"""

__version__ = "0.0.1"

import logging


class ResourceMapper:
    """Mapping abstract resources to real-world resources
       - mapping from different sources (dicts, databases, dynamic)

       ?? shall different resource types have different dictionaries ?
    """

    def __init__(self):
        self._resource_map = {}

    @property
    def resource_map(self):
        return self._resource_map
