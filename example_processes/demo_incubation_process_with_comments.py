# demo incubation process

# specifying resources
cont1 = ContainerResource()
cont2 = ContainerResource()

incubator = DeviceResource()

start1 = cont1.set_start_position()
start2 = cont2.set_start_position()

# initialise the process
init([incubator])

# process steps
move([incubator, cont1],  start1, nest1)
move([incubator, cont2], start2, nest2)

incubate([incubator, cont1], 3600)
incubate([incubator, cont2], 3600)

move([incubator, cont1], nest1, start1)
move([incubator, cont2], nest2, start2)

# shutdow procedures
shutdown([incubator])
