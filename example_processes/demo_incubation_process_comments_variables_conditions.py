# demo incubation process

# specifying resources
cont1 = ContainerResource()
cont2 = ContainerResource()

incubator = DeviceResource()
platereader = DeviceResource()

start1 = cont1.set_start_position()
start2 = cont2.set_start_position()

incubation_duration = 6  # hours

# initialise the process
init([incubator])

# process steps
move([incubator, cont1],  start1, nest1)
move([incubator, cont2], start2, nest2)

incubate([incubator, cont1], incubation_duration, unit="h")
incubate([incubator, cont2], incubation_duration, unit="h")

curr_abs = absorbance([platereader, cont1], 600)  # 600nm

if curr_abs > 3.0:
    move([incubator, cont1], nest1, start1)
    move([incubator, cont2], nest2, start2)

else:
    incubate([incubator, cont1], incubation_duration, unit="h")
    incubate([incubator, cont2], incubation_duration, unit="h")

    move([incubator, cont1], nest1, start1)
    move([incubator, cont2], nest2, start2)

# shutdow procedures
shutdown([incubator])
